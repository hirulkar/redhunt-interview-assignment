import '../App.css';
import { useState, useEffect } from 'react';
import { Card, Spin } from 'antd';
import { HeartOutlined, HeartFilled, MailOutlined, PhoneOutlined, DribbbleOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import { useSelector, useDispatch } from "react-redux"
import { userAction } from "../Redux/Action/userAction"

function UserDetails() {
    const { Meta } = Card;
    const dispatch = useDispatch();
    const [user, setUser] = useState({})
    const storeUserList = useSelector((state) => state.user);
    const [selectLike, setSelectLike] = useState("")

    // set User details in user variable
    useEffect(() => {
        setTimeout(() => {
            setUser(JSON.parse(window.localStorage.getItem("user_data")))
        }, 100);
    }, [])

    // update user details on selectLike variable
    useEffect(() => {
        setUser(user)
    }, [selectLike])

    // update user favorites and update store data
    const addFavorites = (like, name) => {
        setSelectLike(name)
        if (storeUserList.length) {
            for (let i = 0; i < storeUserList.length; i++) {
                if (storeUserList[i].id == user.id) {
                    storeUserList.splice(i, 1, user);
                }
            }
            dispatch(userAction(storeUserList))
        }
    }

    return (
        <div>
            {user ? 
            <Card
                style={{ margin: "16px auto", width: "350px" }}
                cover={
                    <div className="imagesBg">
                        <img
                            alt="example"
                            src={user.img}
                        />
                    </div>}
                actions={[
                    user.favorites ?
                        <HeartFilled key="link" style={{ fontSize: '20px', color: 'red' }} onClick={() => addFavorites(user.favorites = !user.favorites, user.name)} />
                        : <HeartOutlined key="like" style={{ fontSize: '20px', color: 'red' }} onClick={() => addFavorites(user.favorites = !user.favorites, user.username)} />,
                ]}
            >
                <Meta title={user.name} />
                <div className="user_details">
                    <MailOutlined style={{ fontSize: '20px' }} />
                    <p>{user.email}</p>
                </div>
                <div className="user_details">
                    <PhoneOutlined style={{ fontSize: '20px' }} />
                    <p>{user.phone}</p>
                </div>
                <div className="user_details">
                    <DribbbleOutlined style={{ fontSize: '20px' }} />
                    <p>http://{user.website}</p>
                </div>
            </Card>
            : 
            <div className="loader">
                <Spin size="large" />
            </div>
            }
        </div>
    )
}
export default UserDetails