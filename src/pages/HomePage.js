import '../App.css';
import axios from "axios"
import { useState, useEffect } from 'react';
import { Form, Spin, Input, Modal, Row, Col, Card } from 'antd';
import { EditOutlined, HeartOutlined, HeartFilled, DeleteFilled, MailOutlined, PhoneOutlined, DribbbleOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux"
import { userAction } from "../Redux/Action/userAction"

function HomePage() {
    const history = useHistory();
    const dispatch = useDispatch();
    let [newSelectedUser, setSelectedUser] = useState({});
    const [form] = Form.useForm();
    const [visible, setVisible] = useState(false);
    const [selectedIndex, setSelectedIndex] = useState(null);
    const storeUserList = useSelector((state) => state.user);
    const [userList, setUserList] = useState([])
    const [favorites, setFavorites] = useState("")
    const { Meta } = Card;
    const style = { padding: '10px' };

    // update user details on popup
    const onCreate = (values) => {
        // console.log('Received values of form: ', values);
        userList[selectedIndex].name = values.name;
        userList[selectedIndex].email = values.email;
        userList[selectedIndex].phone = values.phone;
        userList[selectedIndex].website = values.website;

        // update user list on setFavorites variable
        setFavorites(values.name)
        setVisible(false);
    };

     // get users data loading page
    useEffect(() => {
        setTimeout(() => {
            // check user data in store and set
            if (!storeUserList.length) {
                getUser();
            } else {
                setUserList(storeUserList);
            }
        }, 100);
    }, [])

    // update user data on change keys
    useEffect(() => {
        // console.log(storeUserList);
        setUserList(userList);
        dispatch(userAction(userList))
    }, [favorites])

    const getUser = () => {
        axios.get("https://jsonplaceholder.typicode.com/users")
            .then(response => {
                for (let i = 0; i < response.data.length; i++) {
                    response.data[i].favorites = false;
                    response.data[i].img = `https://avatars.dicebear.com/v2/avataaars/${response.data[i].username}.svg?options[mood][]=happy` // create user img url on username
                    // response.data[i].img = await getUserImages(response.data[i].username, i)

                }
                dispatch(userAction(response.data))
                setUserList(response.data);
            })
            .catch(err => {
                console.log(err)
            })
    }

    const deleteUser = (item, index) => {
        userList.splice(index, 1);
        setFavorites(item.username)
    };

    const editUser = (item, index) => {
        setSelectedIndex(index)
        setSelectedUser({
            name: item.name,
            email: item.email,
            phone: item.phone,
            website: item.website
        })
        setVisible(true);
    };

    const addFavorites = (item, name) => {
        setFavorites(name)
    }

    const onCancelPopup = () => {
        let obj = {};
        setSelectedUser(obj);
        console.log(newSelectedUser)
        setVisible(false);
    }

    const openUserDetails = (user) => {
        window.localStorage.setItem("user_data", JSON.stringify(user));
        history.push("/user")
    }

    // reset form data
    useEffect(() => form.resetFields(), [newSelectedUser]);

    // const getUserImages = (username, index) => {
    //   return axios.get(`https://avatars.dicebear.com/v2/avataaars/${username}.svg?options[mood][]=happy`).then(response => {
    //    return response.data
    //   }).catch(err => {
    //     console.log(err)
    //   })
    // }

    return (
        <div className="HomePage">
            {userList.length ? 
                <Row>
                    {userList.map((item, index) => {
                        return (
                            <Col style={style} key={index} xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 6 }} className="gutter-row">
                                <Card
                                    style={{ margin: "16px auto" }}
                                    cover={
                                        <div className="imagesBg">
                                            <img
                                                alt="example"
                                                src={item.img}
                                            />
                                        </div>}
                                    actions={[
                                        item.favorites ?
                                            <HeartFilled key="unlike" style={{ fontSize: '20px', color: 'red' }} onClick={() => addFavorites(item.favorites = !item.favorites, item.username)} />
                                            : <HeartOutlined key="favorites" style={{ fontSize: '20px', color: 'red' }} onClick={() => addFavorites(item.favorites = !item.favorites, item.name)} />,
                                        <EditOutlined key="edit" style={{ fontSize: '20px' }} onClick={() => editUser(item, index)} />,
                                        <DeleteFilled key="ellipsis" style={{ fontSize: '20px' }} onClick={() => deleteUser(item, index)} />,
                                    ]}
                                >
                                    <Meta title={item.name} style={{ cursor: "pointer" }} onClick={() => openUserDetails(item)} />
                                    <div className="user_details">
                                        <MailOutlined style={{ fontSize: '20px' }} />
                                        <p>{item.email}</p>
                                    </div>
                                    <div className="user_details">
                                        <PhoneOutlined style={{ fontSize: '20px' }} />
                                        <p>{item.phone}</p>
                                    </div>
                                    <div className="user_details">
                                        <DribbbleOutlined style={{ fontSize: '20px' }} />
                                        <p>http://{item.website}</p>
                                    </div>

                                </Card>
                            </Col>
                        )
                    })}
                </Row>
                : 
                <div className="loader">
                    <Spin size="large" />
                </div>
            }

            <Modal
                visible={visible}
                title="User Details"
                okText="Ok"
                cancelText="Cancel"
                onCancel={() => onCancelPopup()}
                onOk={() => {
                    form
                        .validateFields()
                        .then((values) => {
                            onCreate(values);
                        })
                        .catch((info) => {
                            console.log('Validate Failed:', info);
                        });
                }}
            >
                <Form
                    form={form}
                    layout="vertical"
                    name="form_in_modal"
                    initialValues={newSelectedUser}
                >
                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[
                            {
                                required: true,
                                message: 'Please input the Name of collection!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="Email"
                        rules={[
                            {
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                            {
                                required: true,
                                message: 'Please input your E-mail!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        label="Phone"
                        rules={[
                            {
                                required: true,
                                message: 'Please input the E-mail of collection!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="website"
                        label="website"
                        rules={[
                            {
                                required: true,
                                message: 'Please input the E-mail of collection!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                </Form>
            </Modal>

        </div>
    );
}

export default HomePage;
