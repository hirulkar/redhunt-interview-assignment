import './App.css';
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import HomePage from "./pages/HomePage"
import UserDetails from "./pages/UserDetails"

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
            <Route exact path='/'>
              <HomePage/>
            </Route>
            <Route exact path='/user'>
              <UserDetails/>
            </Route>
          </Switch>
      </Router>
    </div>
  );
}

export default App;
