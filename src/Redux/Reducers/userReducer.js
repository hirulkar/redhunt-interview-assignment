import { USER_LIST } from '../Action/userAction';


export default function userReducer(state = [], {type, payload}){
    switch(type){
        case USER_LIST:
            return payload;
        default:
            return state;
    }
}