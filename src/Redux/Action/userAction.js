export const USER_LIST= "userReducer:USER_LIST";

export function userAction(data){
    return {
        type: USER_LIST,
        payload: data
    }
}